<!--bot:validateCode-->

> Attention: automated service
---
# Code Validation

## With this issue you can validate a code and get the service name

* **System Code**: <!-- pattern: 00000000000 -->
* **System Name**: <!-- auto-filled -->

---
*doc*: https://my-doc.com